# ChangeLog

All notable changes to this project are documented in this file.

## [0.3.0] - 2020-05-27

- Add requirement on fluentd with a set of default values
- Add requirement on landb-sync
- Add requirement on prometheus-cern

## [0.1.1] - 2020-05-06

- Update nvidia-gpu requirement to 0.3.0
- Fix gitlab-ci with appropriate cern repo

## [0.1.0] - 2020-04-30

- First Release
